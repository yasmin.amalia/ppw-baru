from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, books, subscribe
from .models import Status, Subscribe
from .forms import Status_Form, Subscribe_Form
from django.db import IntegrityError

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class Lab6UnitTest(TestCase):
    #test for landing page
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_landing_page_content_is_written(self):
        response = self.client.get('/')
        string = response.content.decode("utf-8")
        self.assertIn("Hello, Apa Kabar?", string)

    def test_model_can_create_new_status(self):
        #Creating a new status
        new_status = Status.objects.create(name='Yasmin', status='ppw bikin stress')

        #Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'name': '', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    
    def test_story6_post_success_and_render_the_result(self):
        test = 'Coba-coba'
        response_post = Client().post('/', {'status':test})
        self.assertEqual(response_post.status_code, 200)

    #test for profile page
    def test_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_content_is_written(self):
        response = self.client.get('/profile/')
        string = response.content.decode("utf-8")
        self.assertIn("Yasmin Amalia.", string)

    #test for books page
    def test_url_books_is_exist(self):
        response=Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_books_using_views_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_books_return_jsonresponse(self):
        response = Client().get('books/api/library/')
        self.assertEqual(response.status_code,404)

class Lab6SeleniumTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Lab6SeleniumTest,self).setUp()
    
    def test_input_status(self):
        selenium = self.selenium
        #Opening the link we want to test
        selenium.get('http://website-yasmin.herokuapp.com')
        
        #Find the form element
        name = selenium.find_element_by_id('name-form')
        status = selenium.find_element_by_id('status-form')
        
        submit = selenium.find_element_by_id('buttons')

        #Fill the form with data
        name.send_keys('PEPEW')
        status.send_keys('Coba-coba')

        #Submitting the form
        submit.send_keys(Keys.RETURN)

        #Check if the form is filled with "Coba-coba"
        self.assertIn("Coba-coba", selenium.page_source)
    
    def test_title(self):
        selenium = self.selenium
        #Opening the link we want to test
        selenium.get('http://website-yasmin.herokuapp.com')

        #Check the title
        self.assertIn("Little Journey", selenium.title)

    def test_tag_image_in_profile_page(self):
        selenium = self.selenium
        #Opening the link we want to test
        selenium.get('https://website-yasmin.herokuapp.com/profile/')

        #Check the image
        image = selenium.find_elements_by_tag_name('img')
        self.assertTrue(image, selenium.page_source)
    
    def test_button_css_in_landing_page(self):
        selenium = self.selenium
        selenium.get('https://website-yasmin.herokuapp.com')

        #Find element
        button = selenium.find_element_by_id('buttons')
        css = button.value_of_css_property('background-color')

        #Check css
        self.assertEqual('rgba(220, 20, 60, 1)', css)
    
    def test_h1_css_in_profile_page(self):
        selenium = self.selenium
        selenium.get('https://website-yasmin.herokuapp.com/profile/')

        #Find element
        heading1 = selenium.find_element_by_tag_name('h1')
        css = heading1.value_of_css_property('font-size')

        #Check css
        self.assertEqual('57px', css)
    
    def tearDown(self):
        self.selenium.quit()
        super(Lab6SeleniumTest, self).tearDown()

class Story10UnitTest(TestCase):
        #test for subscribe page
    def test_url_subscribe_is_exist(self):
        response=Client().get('/subscribe/')
        self.assertEqual(response.status_code,200)

    def test_subscribe_using_views_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)
    
    
    def test_subsform_validation_for_blank_items(self):
        form = Subscribe_Form(data={'name': '', 'email': '', 'password': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'],
            ["This field is required."]
        )
    
    def test_post_success_and_render_the_result(self):
        name='ppw'
        email='ppw@pepewe.com'
        password = 'pepewe2018'
        response_post=Client().post('/add_subscriber/',{'name':name,'email':email,'password':password})
        self.assertEqual(response_post.status_code,200)

        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response,'subscribe.html')
    
    # def test_post_error_and_does_not_exists_in_the_database(self):
    #     Client().post('/add_subscriber/',{'name': '', 'email': '', 'password': ''})
    #     count_data = Subscribe.objects.all().count()
    #     self.assertEqual(count_data, 0)

    def test_model_can_create_new_subscribe(self):
        #Creating a new subscriber
        new_subscribe = Subscribe.objects.create(name='yasmin', email='yasmin@ppw.com', password='ppw2018')

        #Retrieving all available activity
        counting_all_available_subscriber = Subscribe.objects.all().count()
        self.assertEqual(counting_all_available_subscriber, 1)

    def test_JSON_object_failed_created(self):
        create_model = Subscribe.objects.create(name='ppw',email='ppw@pepewe.com',password='pepewe2018')
        self.assertRaises(IntegrityError)

