var counters = 0;
var emailIsValid = false;

$(document).ready(function(){
    var changed = true;
    $("#themes").click(function(){
        if(changed){
            console.log("theme changed");
            $("h1").css('color', 'crimson');
            $("h2").css('color', 'crimson');
            $("h4").css('color', '#22223B');
            $("#buttons").css('background-color', '#22223B');
            $("th").css('background-color', '#22223B');
            $("td").css('color', '#22223B');
            $("#profile p").css('color', 'crimson');
            $(".btn").css('background-color', '#22223B');
            $(".accordion").css('color', 'crimson');
            $(".panel").css('color', 'crimson');
            changed = false;
        }else{
            console.log("back to original");
            $("h1").css('color', '#22223B');
            $("h2").css('color', '#22223B');
            $("h4").css('color', 'crimson');
            $("#buttons").css('background-color', 'crimson');
            $("th").css('background-color', 'crimson');
            $("td").css('color', 'crimson');
            $("#profile p").css('color', '#22223B');
            $(".btn").css('background-color', 'crimson');
            $(".accordion").css('color', '#22223B');
            $(".panel").css('color', '#22223B');
            changed = true;
        }
        
    });

    // architecture
    $("#architecture").click(function() {
        process('architecture');
    });
    
    // quilting
    $("#quilting").click(function() {
        process('quilting');
    });

    // comic
    $("#comic").click(function() {
        process('comic');
    });

    // counter
    $(document).on('click', '#buttons', function() {
        if( $(this).hasClass('clicked') ) {
            counters -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counters = counters + 1;
        }
        $('.counters').html(counters);
    });

    // Accordion
    $(".accordions").find(".accordion").click(function(){
        //Expand or collapse this panel
        console.log("bisa accordion")
        $(this).next().slideToggle('fast');
  
        //Hide the other panels
        $(".panel").not($(this).next()).slideUp('fast');
    });

    // Positioning the overlay on the center
    $.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (
            ($(window).height() - $(this).outerHeight()) / 2) + 
            $(window).scrollTop()) + "px"
        );
        this.css("left", Math.max(0, (
            ($(window).width() - $(this).outerWidth()) / 2) + 
            $(window).scrollLeft()) + "px"
        );
        return this;
    }
    
    // Showing the overlay
    $("#overlay").show();
    $("#overlay-content").show().center();

    // Closing the overlay
    setTimeout(function(){    
        $("#overlay").fadeOut();
    }, 3000);

    // susbscribe
    $('input').focusout(function(){
        checkAll();
    });

    $('#email-form').keyup(function(){
        checkEmail();
    });
    
    $('input').focusout(function(){
        checkEmail();
    });

    $('#submit').click(function(){
        data = {
            'name': $('#subsname-form').val(),
            'email': $('#email-form').val(),
            'password': $('#pass-form').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type: 'POST',
            url: '/add_subscriber/',
            data: data,
            dataType: 'json',
            success: function(data){
                alert(data['message']);
                document.getElementById('subsname-form').value='';
                document.getElementById('email-form').value= '';
                document.getElementById('pass-form').value='';

                $('#status').html('');
                checkAll();
            }
        })
    });

    // subscriber list

    $.ajax({
        url:'/subscriber_data/',
        type:'GET',
        dataType:'json',
        success: function(data){
            var row = ('<tr>');
            for(var i = 1;i<data.datas.length;i++){
                row += '<td scope ="col">' + i +'</td>';
                row+= '<td scope ="col">' + data.datas[i-1].name +'</td>';
                row+= '<td>'+data.datas[i-1].email+'</td>';
                row+= '<td>' +'<button class="btn btn-default" id="unsubs-button" onClick="unsubscribe('+data.datas[i-1].pk+')" type="submit">Unsubscribe</button></td></tr>';
            }
            console.log(row)
            $('#subscriber-table').append(row);
        }
    });

    // unsubscribe
    $('#unsubs-button').click(function(){
        
    });


});

function process(tema) {
    //bookshelves

    $.ajax({
        type : "GET",
        url : 'api/library/' + tema,
        dataType : 'json',
        success : function(data) {
            $('td').remove(print);        
            var print ='<tr>';
                for (var i = 1; i < data.data.length; i++) {
                    print += '<td scope ="col">' + i +'</td>';
                    print += '<td>'+'<img src=\"' + data.data[i-1].thumbnail + '\">'+'</td>';
                    print += '<td>'+data.data[i-1].title+'</td>';
                    print += '<td>'+data.data[i-1].authors +'</td>';
                    print +='<td> <button id="buttons" type="button" class="btn btn-outline-light" data-toggle="tooltip" data-placement="top" title="Click on the icon"><i id="star"class ="fa fa-star" style="font-size:24px"></i></button> </td></tr>';
                }
            $('tbody').append(print);
        }
    });
}

// fungsi untuk mengecek secara keseluruhan
function checkAll(){
    if(emailIsValid === true && $('#subsname-form').val() !== '' && $('#pass-form').val() !== ''){
        $('#submit').prop('disabled', false);
    }else{
        $('#submit').prop('disabled', true);
    }
}

// fungsi untuk validasi email
function checkEmail(){
    data = {
        'email': $('#email-form').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }

    $.ajax({
        type: "POST",
        url: '/check_email/',
        data: data,
        dataType: 'json',
        success: function(data){
            $('#status').html('');

            if(data['status'] === 'fail'){
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#status').append('<small>' + data["message"] + '</small>');
            }else{
                emailIsValid = true;
                checkAll();
                $('#status').append('<small>' + data["message"] + '</small>');
            }
        }
    });
}

// unsubscribe functio

function unsubscribe(pk){
    console.log(pk)
    $.ajax({
        method:'POST',
        url:'/unsubscribe/',
        data:{'pk':pk},
        success:function(){
            alert("You are unsubscribed! :(")
            window.location.reload();
        }
    });
}
