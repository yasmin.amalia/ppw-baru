from django import forms

class Status_Form(forms.Form):
    error_message = {
        'required': 'Please fill in'
    }

    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Your Name',
        'id': 'name-form'
    }
    stat_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': "What's on your mind?",
        'id': 'status-form'
    }

    name = forms.CharField(label='Name', required=True, max_length=50,
    widget=forms.TextInput(attrs=name_attrs))
    status = forms.CharField(label='Status', required=True, max_length=300,
    widget= forms.TextInput(attrs=stat_attrs))

class Subscribe_Form(forms.Form):
    error_message = {
        'required': 'Please fill in'
    }

    subsname_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Your Name',
        'id': 'subsname-form'
    }
    email_attrs = {
        'type': 'type',
        'class': 'form-control',
        'placeholder': "yourname@mail.com",
        'id': 'email-form'
    }
    pass_attrs = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': "Type your password here",
        'id': 'pass-form'
    }

    name = forms.CharField(label='Name', required=True, max_length=50,
    widget=forms.TextInput(attrs=subsname_attrs))
    email = forms.CharField(label='Email', required=True, max_length=100,
    widget= forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='Password', required=True, max_length=50,
    widget=forms.PasswordInput(attrs=pass_attrs))