"""PPWwebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import index, profile, books, books_api, subscribe, check_email, add_subscriber, subscriber_data, unsubscribe, login, logout

app_name = 'story6'
urlpatterns = [
    path('', index, name='index'),
    path('profile/', profile, name = 'profile'),
    path('books/', books, name = 'books'),
    path('books/api/library/<str:tema>/', books_api, name='books_api'),
    path('subscribe/', subscribe, name='subscribe'),
    path('add_subscriber/', add_subscriber, name='add_subscriber'),
    path('check_email/', check_email, name='check_email'),
    path('subscriber_data/', subscriber_data, name='subscriber_data'),
    path('unsubscribe/', unsubscribe, name='unsubscribe'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]
