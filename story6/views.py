from django.shortcuts import render
from .forms import Status_Form, Subscribe_Form
from .models import Status, Subscribe
from django.http import JsonResponse, HttpResponse
import urllib.request, json
import requests
from django.core.validators import validate_email
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}
def index(request):
    response['status_form'] = Status_Form
    the_status = Status.objects.all()
    response['Status_Form'] = the_status

    form = Status_Form(request.POST or None)
    if(request.method == 'POST'  and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['status'] = request.POST['status']
    
        the_status = Status(name=response['name'], status=response['status'])
        the_status.save()
    
        return render(request, 'hello.html', response)
    else:
        return render(request, 'hello.html', response)

def profile(request):
    return render(request, 'profile.html')

def books(request):
    return render(request, 'books.html')

def books_api(request, tema):
    api = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + tema).json()
    book_items = []

    for i in api['items']:
        data = {}
        data['thumbnail'] = i['volumeInfo']['imageLinks']['thumbnail'] if 'imageLinks' in i['volumeInfo'].keys() else 'Not available'
        data['title'] = i['volumeInfo']['title']
        data['authors'] = ", ".join(i['volumeInfo']['authors']) if 'authors' in i['volumeInfo'].keys() else 'Not available'
        book_items.append(data)
    return JsonResponse({"data" : book_items})

def subscribe(request):
    response['subscribe_form'] = Subscribe_Form
    html = 'subscribe.html'
    return render(request, html, response)

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message' : 'E-mail format is wrong!',
            'status' : 'fail'
        })
    
    existed = Subscribe.objects.filter(email=request.POST['email'])

    if(existed):
        return JsonResponse({
            'message': "E-mail is already registered",
            'status': 'fail'
        })
    else:
        return JsonResponse({
            'message': "E-mail is valid",
            'status': 'success'
        })

def add_subscriber(request):
    if (request.method == "POST"):
        subscribe = Subscribe(
            name = request.POST['name'],
            email = request.POST['email'],
            password = request.POST['password']   
        )
        subscribe.save()

        return JsonResponse({
            'message':'Subcription success! :)'
        }, status = 200)

    else:
        return JsonResponse({
            'message':"Subcription failed! :("
        }, status = 403)

def subscriber_data(request):
    data = list(Subscribe.objects.all().values('pk', 'name', 'email', 'password'))
    return JsonResponse({'datas': data})

@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        data = request.POST['pk']
        Subscribe.objects.filter(pk=data).delete()
        return HttpResponse({'status':'Success'})
    else:
        return HttpResponse({'status':'Failed to load page'})

@csrf_exempt
def auth_code(request):
    return JsonResponse({"user" : code})

